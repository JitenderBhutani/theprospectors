<br>
<b><u>PREPARATION OF LEASE AGREEMENT</b></u><br>
<br>
A rent agreement is a legal document which binds the landlord and the tenant to comply with the mutually-agreed conditions. It is the most crucial document in case of a dispute between the two.<br>
Therefore, utmost care should be taken in drafting the document. It should state all terms and conditions clearly to avoid any dispute in future. "Take the help of a legal expert to draft the lease agreement. Try doing a reference check and, most important, ensure that a police verification done. Its is in the best interest of all stakeholders" says Ashok Mohanani, CMD, EKTA World.<br>
<br>
<b>These are the points one must consider while finalising the agreement.</b><br>
	1) Name, address, father's name of both the tenant and the landlord should be clearly mentioned.<br>
	2) It should be verified that the lessor is the legal owner of the property or a person duly authorised by him or a person authorised by a court to enter into such a contract.<br>
	3) The rent should be clearly mentioned (also whether it includes maintenance fee). The quantum of increase in rent and from which date should also be made clear. The mode of payment, whether in cheque or cash, plus the date of payment, is also important. Any interest to be paid in case of delayed payment should also be stated clearly.<br>
	4) The period of tenancy should be clearly mentioned. The security amount and the lock-in period are also important. How the amount will be refunded or whether it will be adjusted in the advance rent should also be clearly mentioned.<br>
	5) Who will pay water, electricity and maintenance charges is also important.<br>
	6) In case the flat is furnished, a list of fittings and fixtures should be made and penalty for damage decided in advance. The landlord should also check plumbing, electrical, sanitary fittings, etc, and mention their condition in the agreement. Details of the condition of walls, ceilings and rooftop should also be mentioned so that there is no dispute over damage to the house, if there is any.<br>
	7) The purpose of tenancy should be clearly written- whether it will be used for commercial or residential purpose.<br>
	8) The process for premature termination of the lease.<br>
	9) The agreement must specify the availability of facilities like common passage, roof, park, swimming pool, car parking, library, club, gymnasium, etc, besides the demarcated property, and charges payable, if any, for these.<br>
<br>
This list is not exhaustive. The landlord and the tenant can add as many conditions as they want.<br>
<br>
<b>REGISTER THE LEASE</b><br>
Section 17 of the Registration Act makes it mandatory to get the rent agreement registered if the lease period is more than 11 months.<br>
After preparing the lease agreement, the most important task is to get it registered. As per Rajeev Aggarwal, partner, R. A. Law Co. Advocates & Consultants, every lease agreement should be registered because only then it can be used as evidence in the court in case of any litigation. However, as per Section 17 of the Registration Act 1908, it is compulsory to get the agreement registered only if the lease period is more than 11 months. For registration, stamp duty and registration fee have to be paid.<br>
<br>
<b>POLICE VERIFICATION</b><br>
This process helps in background check of the tenant. Not doing this is a punishable offence under Section 188 of the Indian Penal Code. This lowers the risk that the house is not being rented out to a personal with criminal background. For this the landlord simply has to fill the verification form and submit it to the local police station along with identification proof of the tenant. The forms are available online on websites of state police departments.<br>
Apart from the above, the landlord should periodically visit the premises to check whether the tenant is violating any condition or if he has sublet the property. If yes, he can demand that he vacate the house. If the tenant refuses to vacate, the landlord can approach the authority which oversees disputes related to rent with all the documents.<br>
