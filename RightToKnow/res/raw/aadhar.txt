<br>
1. Depending on the field to be updated, send self-signed (self-attested) supporting documents as per the Valid Documents List attached in Annexure I.	<br>
	Name Correction/Update - Requires submission of any one of the Proof of Identity(PoI) listed in "Supported PoI Documents Containing Name and Photo for Name Corrections/Update"	<br>
	a.Date of Birth Correction (DoB) - Requires submission of any one of the DoB listed in "Supported Proof of Date of Birth Documents"	<br>
	b.Address Corrections/Change - Requires submission of any one of the Proof of Address (PoA) listed in "Supported Proof of Address (PoA) Documents Containing Name and Address"<br>
<br>
2. In case a child is below five years, parent/guardian can sign the copies of documents. In all other cases, the resident must sign the copies of documents themselves<br>
<br>
3. Once you complete your request, you will receive a Update Request Number (URN). Mention your Update Request Number (URN) and Aadhaar Number on all copies of documents. Mention Type of Document (PoI/PoA/DoB) on the copy of the document<br>
<br>
4. Mention the Update Request Number (URN) on the envelope. Mark the envelope as " Aadhaar Update/Correction " on top. Seal the envelope properly.<br>
<br>
5. Note necessary details on this page and then complete the request. Take a print of the Acknowledgement after completing the request on-line<br>
<br>
6. Send the Acknowledgement along with the supporting documents to one of the addresses given in Annexure II<br>
<br>
												<b><u>Annexure I</b></u><br>
<br>
<b>Supported Proof of Identity(PoI) Documents Containing Name and Photo for Name Corrections</b><br>
<br>
1.	Passport<br>
2.	PAN Card<br>
3.	Ration/ PDS Photo Card<br>
4.	Voter ID<br>
5.	Driving License<br>
6.	Government Photo ID Cards/ service photo identity card issued by PSU<br>
7.	NREGS Job Card<br>
8.	Photo ID issued by Recognized Educational Institution<br>
9.	Arms License<br>
10.	Photo Bank ATM Card<br>
11.	Photo Credit Card<br>
12.	Pensioner Photo Card<br>
13.	Freedom Fighter Photo Card<br>
14.	Kissan Photo Passbook<br>
15.	CGHS / ECHS Photo Card<br>
16.	Address Card having Name and Photo issued by Department of Posts<br>
17.	Certificate of Identify having photo issued by Gazetted Officer or Tehsildar on letterhead<br>
18.	Disability ID Card/handicapped medical certificate issued by the respective State/UT<br>
19.	Marriage Certificate<br>
20.	Proof of Marriage document issued by the Registrar<br>
21.	Gazette Notification<br>
22.	Legal Name Change Certificate<br>
(For above documents, where original document does not have photo, the photocopy/scan of the documents must be taken along with the Resident's photo)<br>
<br>
<br>
<b>Supported Proof of Date of Birth (DoB) Documents :</b><br>
1.	Birth Certificate<br>
2.	SSLC Book/Certificate<br>
3.	Passport<br>
<br>
	<br>
<b>Supported Proof of Address (PoA) Documents Containing Name and Address</b><br>
1.	Passport<br>
2.	Bank Statement/ Passbook<br>
3.	Post Office Account Statement/Passbook<br>
4.	Ration Card<br>
5.	Voter ID<br>
6.	Driving License<br>
7.	Government Photo ID cards/ service photo identity card issued by PSU<br>
8.	Electricity Bill (not older than 3 months)<br>
9.	Water bill (not older than 3 months)<br>
10.	Telephone Landline Bill (not older than 3 months)<br>
11.	Property Tax Receipt (not older than 3 months)<br>
12.	Credit Card Statement (not older than 3 months)<br>
13.	Insurance Policy<br>
14.	Signed Letter having Photo from Bank on letterhead<br>
15.	Signed Letter having Photo issued by registered Company on letterhead<br>
16.	Signed Letter having Photo issued by Recognized Educational Instruction on letterhead<br>
17.	NREGS Job Card<br>
18.	Arms License<br>
19.	Pensioner Card<br>
20.	Freedom Fighter Card<br>
21.	Kissan Passbook<br>
22.	CGHS / ECHS Card<br>
23.	Certificate of Address having photo issued by MP or MLA or Gazetted Officer or Tehsildar on letterhead<br>
24.	Certificate of Address issued by Village Panchayat head or its equivalent authority (for rural areas)<br>
25.	Income Tax Assessment Order<br>
26.	Vehicle Registration Certificate<br>
27.	Registered Sale / Lease / Rent Agreement<br>
28.	Address Card having Photo issued by Department of Posts<br>
29.	Caste and Domicile Certificate having Photo issued by State Govt.<br>
30.	Disability ID Card/handicapped medical certificate issued by the respective State/UT Governments/Administrations<br>
31.	Gas Connection Bill (not older than 3 months)<br>
32.	Passport of Spouse<br>
33.	Passport of Parents(in case of Minor)<br>
<br>
<b><u>Contact Us:</u></b>
<br>
<b>Address :</b> 
Unique Identification Authority of India (UIDAI)
Government of India (GoI)
3rd Floor, Tower II
Jeevan Bharati Building
Connaught Circus
New Delhi - 110001
<br>
<b>Tel :</b>1947
<br>
<b>Email:</b> help@uidai.gov.in
<br>
<a href="https://ssup.uidai.gov.in/document-list">More Info</a>
