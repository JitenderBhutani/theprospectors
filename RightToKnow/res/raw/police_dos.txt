<br>
1.  Be polite, but straightforward.<br>
<br>
2.	Avoid the thana. You do not have to go to the police station if an officer asks you, unless you are formally being arrested. Otherwise, he needs a written summons to get you to come to the police station.<br>
<br>
3.	A woman can only be taken to a thana in the daytime (before 6 PM), and a female officer needs to be present.<br>
<br>
4.	If your crime is one where you can get away by paying a fine, ask the cops to make a challan and pay the legit amount, which is usually very nominal<br>
<br>
5.	Ensure that the identity of the police officer effecting arrest is clearly indicated by means of a name plate and rank.<br>
<br>
6.	Ensure that a relative or a friend of the arrestee is informed about the fact of arrest and the place where he/she is being detained<br>
<br>
7.	Ensure that if some injuries are found on the body of the arrested person at the time of arrest, the same are specified in the Arrest Memo and the arrested person is medically examined.<br>
<br>
8.	Protect the human dignity of the person being arrested. Public display or parading of the person arrested should not be permitted.<br>
<br>
9.	Conduct search of the arrested person with due respect to his/her human dignity. Avoid unnecessary use of force and respect his/her right to privacy.Searches of women should be conducted only by other women, with strict regard to decency<br>
<br>
10.	Ensure that a written order is sent to any person who may be required to come to the police station for the purpose of questioning.<br>
<br>
11.	Ensure that family members friends of any person detained by the police are aware of his whereabouts.<br>
<br>
12.	Ensure that whenever any person is detained in the police station, proper entry is made in the General Diary.<br>
<br>
13.	Report to the SHO any instance of domestic violence that comes to your notice. <br>
<br>
14.	Be sympathetic to women victims of crime, especially rape and molestation, and give due respect to their privacy.<br>
<br>
15.	Treat Children politely and interrogate them preferable while dressed in civil clothes.<br>
<br>
16.	It is the duty of the children to maintain their old mother and father if they can not maintain themselves<br>
<br>
17.	Give due respect whenever a senior citizen visits the PS and attend to his/her problem on priority.<br>
<br>
18.	Deal with the matters of minorities with due caution and in a sensitive manner.<br>
<br>
19.	While visiting houses, places of worship, or institutions of members of a minority community, give due respect to their sentiments and religious practices.<br>
<br>
20.	Take suitable action (under various Acts) if the labourers belong to the weaker sections of the society like children, women, minorities, and SC/ST.<br>
<br>
21.	When faced with armed criminals/ offenders, challenge them first and ask them to surrender.<br>
<br>
<a href="http://nhrc.nic.in/Documents/Publications/guideline_for_poloce_personnel_on_various_HR_issues_Eng.pdf">More Info</a>
