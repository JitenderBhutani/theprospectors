<br>
<b>1.	Copy of Proof of Residence: This is required to validate the address you provide while filling the Form No. 6. Valid of address proofs are as follows:</b><br>
	i.	Electricity/Water Bill<br>
	ii.	Statement issued by Bank<br>
	iii.	MTNL / BSNL Telephone bills<br>
	iv.	Bank Passbooks<br>
	v.	Passport<br>
	vi.	Driving License<br>
	vii.	Ration Card<br>
<br>
<b>2.	Copy of Proof of Age & Identity: This is required to validate the Date of Birth as well as your Name and Identity you filled in on the Form No. 6. Valid Documents for ID & Age Proof are:</b><br>
	i.	10th Standard passing certificate<br>
	ii.	Birth certificate issued by the civic agency of your state or any other appropriate document signifying the voter's age.<br>
	iii.School Leaving Certificate Specifying Age<br>
	iv.	PAN Card<br>
	v.	Driving License<br>
	vi.	Passport<br>
	vii.	Senior Citizen Card (if applicable)<br>
	viii.	Aadhar Card<br>
	ix.	Kissan Card<br>
<br>
<b>3.	2 recent passport size photographs, which would appear on the Elector's Photo Identity Card.</b><br>
<br>
<a href="http://www.nvsp.in/">More Info</a>