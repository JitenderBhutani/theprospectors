<br>
To submit the application, you need to upload several documents to the PAN Services Unit website. You will need to upload a proof of identity, address and date of birth, along with a scanned passport photograph. Start by making a scan of any one document per section, for each section below:<br>
<br>
<b>1. Proof of identity </b><br>
<br>
	A copy of any one of these documents is acceptable as ID proof for a PAN card:<br>
	<br>
	i.	Aadhaar Card issued by the Unique Identification Authority of India<br>
	ii.	Elector's photo identity card<br>
	iii.	Driving licence<br>
	iv.	Passport<br>
	v.	Ration card having photograph of the applicant<br>
	vi.	Arms license<br>
	vii.	Photo identity card issued by the Central Government or State Government or Public Sector Undertaking<br>
	viii.	Pensioner card having photograph of the applicant<br>
	ix.	Central Government Health Service Scheme Card or Ex-Servicemen Contributory Health Scheme photo card<br>
	x.	Certificate of identity in original signed by a Member of Parliament or Member of Legislative Assembly or Municipal Councillor or a Gazetted officer<br>
	xi.	Bank certificate in original on letter head from the branch (along with name and stamp of the issuing officer) containing attested photograph and bank account number of the applicant<br>
<br>
	<b>2. Address proof</b><br>
<br>
	Along with your photo ID, you'll also have to enclose an address proof in your application. Any of the following documents can be included with your form:<br>
<br>
	i.	Aadhaar Card issued by the Unique Identification Authority of India<br>
	ii.	Elector's photo identity card<br>
	iii.	Driving licence<br>
	iv.	Passport<br>
	v.	Passport of the spouse<br>
	vi.	Post office passbook having address of the applicant<br>
	vii.	Latest property tax assessment order<br>
	viii.	Domicile certificate issued by the government<br>
	ix.	Allotment letter of accommodation issued by Central or State Government of not more than three years old<br>
	x.	Property registration<br>
	xi.	Certificate of address in original signed by a Member of Parliament or Member of Legislative Assembly or Municipal Councillor or a Gazetted officer<br>
	xii.	Employer certificate in original<br>
	<br>
	
	<b>You can also use one of the following documents, if it is less than three months old:</b><br>
	<br>
	i.	Electricity bill<br>
	ii.	Landline telephone or broadband connection bill<br>
	iii.	Water bill<br>
	iv.	Consumer gas connection card or book or piped gas bill<br>
	v.	Bank account statement<br>
	vi.	Depository account statement<br>
	vii.	Credit card statement<br>
<br>
	<b>3. Proof of date of birth</b><br>
<br>
	A copy of any one of these documents should suffice as proof of date of birth:<br>
	<br>
	i.	Birth Certificate issued by the Municipal Authority or any office authorised to issue Birth and Death Certificate by the Registrar of Birth and Deaths or the Indian Consulate as defined in clause (d) of sub-section (1) of section 2 of the Citizenship Act, 1955 (57 of 1955)<br>
	ii.	Pension payment order<br>
	iii.	Marriage certificate issued by Registrar of Marriages<br>
	iv.	Matriculation certificate<br>
	v.	Passport<br>
	vi.	Driving licence<br>
	vii.	Domicile certificate issued by the government<br>
	viii.	Affidavit sworn before a magistrate stating the date of birth<br>
	<br>
	<b>4. Photographs</b><br>
<br>
	You will need two recent passport sized photographs for the PAN card application.<br>
	<br>
	<br>
<b><u>Contact Us:</u></b>
	<br>
<b>Address :</b> 5th Floor, Mantri Sterling,Plot No. 341, Survey No. 997 /8, Model Colony, Near Deep Bungalow Chowk, Pune - 411 016.
<br>
<b>Tel :</b> 020 - 2721 8080
<br>
<b>Fax :</b> 020 - 2721 8081
<br>
<b>Email:</b> tininfo@nsdl.co.in
<br>
<a href="https://www.tin-nsdl.com/pan/documents-req.php">More Info</a>
