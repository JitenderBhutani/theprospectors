/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.example.righttoknow;

public final class R {
    public static final class array {
        public static final int navigation_drawer_items_array=0x7f080000;
        public static final int search_items_array=0x7f080001;
    }
    public static final class attr {
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f060000;
        public static final int activity_vertical_margin=0x7f060001;
    }
    public static final class drawable {
        public static final int aadhar=0x7f020000;
        public static final int add=0x7f020001;
        public static final int bike=0x7f020002;
        public static final int bike2=0x7f020003;
        public static final int car=0x7f020004;
        public static final int cellstyle=0x7f020005;
        public static final int cellstyle2=0x7f020006;
        public static final int disclaimer=0x7f020007;
        public static final int dl=0x7f020008;
        public static final int donts=0x7f020009;
        public static final int dos=0x7f02000a;
        public static final int drawer_shadow=0x7f02000b;
        public static final int faq=0x7f02000c;
        public static final int faq2=0x7f02000d;
        public static final int home=0x7f02000e;
        public static final int homepag2=0x7f02000f;
        public static final int homepage=0x7f020010;
        public static final int house=0x7f020011;
        public static final int ic_drawer=0x7f020012;
        public static final int ic_launcher=0x7f020013;
        public static final int id=0x7f020014;
        public static final int marriage=0x7f020015;
        public static final int noc=0x7f020016;
        public static final int pan=0x7f020017;
        public static final int passport=0x7f020018;
        public static final int penalty=0x7f020019;
        public static final int police=0x7f02001a;
        public static final int police_dos=0x7f02001b;
        public static final int post_logo=0x7f02001c;
        public static final int postofficeicon=0x7f02001d;
        public static final int s1=0x7f02001e;
        public static final int search=0x7f02001f;
        public static final int search2=0x7f020020;
        public static final int splashscreen=0x7f020021;
        public static final int trafficpolice=0x7f020022;
        public static final int transport=0x7f020023;
        public static final int vote=0x7f020024;
    }
    public static final class id {
        public static final int Driving_License_Button=0x7f0b0007;
        public static final int Passport_Button=0x7f0b0008;
        public static final int PoliceDOs=0x7f0b001d;
        public static final int PoliceDonts=0x7f0b001e;
        public static final int action_settings=0x7f0b0045;
        public static final int btnAadhar=0x7f0b000a;
        public static final int btnBike=0x7f0b0032;
        public static final int btnCar=0x7f0b0033;
        public static final int btnID=0x7f0b0040;
        public static final int btnMarriage=0x7f0b000c;
        public static final int btnNOC=0x7f0b0034;
        public static final int btnOthers=0x7f0b003c;
        public static final int btnPAN=0x7f0b0009;
        public static final int btnPolice=0x7f0b003e;
        public static final int btnProperty=0x7f0b003d;
        public static final int btnTrafficFAQ=0x7f0b002d;
        public static final int btnTrafficFine=0x7f0b002c;
        public static final int btnTrafficPolice=0x7f0b003f;
        public static final int btnTransport=0x7f0b0041;
        public static final int btnVote=0x7f0b000b;
        public static final int content_frame=0x7f0b000e;
        public static final int drawer_layout=0x7f0b000d;
        public static final int fragment_container=0x7f0b0021;
        public static final int friend_list=0x7f0b0026;
        public static final int imageView1=0x7f0b003b;
        public static final int imageViewIcon=0x7f0b0042;
        public static final int ivSplashScreen=0x7f0b0027;
        public static final int left_drawer=0x7f0b000f;
        public static final int lvAS=0x7f0b0002;
        public static final int search=0x7f0b0046;
        public static final int searchViewText=0x7f0b0044;
        public static final int svAadhar=0x7f0b0000;
        public static final int svDisclaimer=0x7f0b0003;
        public static final int svDrivingLicense=0x7f0b0005;
        public static final int svMarriage=0x7f0b0010;
        public static final int svPan=0x7f0b0013;
        public static final int svPassport=0x7f0b0015;
        public static final int svPoliceDonts=0x7f0b0018;
        public static final int svPoliceDos=0x7f0b001b;
        public static final int svPropertyBuy=0x7f0b001f;
        public static final int svPropertyRent=0x7f0b0022;
        public static final int svPropertySell=0x7f0b0024;
        public static final int svTrafficPoliceFAQ=0x7f0b0028;
        public static final int svTrafficPoliceFines=0x7f0b002a;
        public static final int svTransportBuy2WheelSecond=0x7f0b002e;
        public static final int svTransportNOC=0x7f0b0035;
        public static final int svTransportbuy4Wheeler=0x7f0b0030;
        public static final int svVote=0x7f0b0037;
        public static final int tableTrafficPoliceFines=0x7f0b002b;
        public static final int textView1=0x7f0b0012;
        public static final int textViewName=0x7f0b0043;
        public static final int tvASFragment=0x7f0b0039;
        public static final int tvASText=0x7f0b003a;
        public static final int tvAadhar=0x7f0b0001;
        public static final int tvDisclaimer=0x7f0b0004;
        public static final int tvDonts=0x7f0b0017;
        public static final int tvDos=0x7f0b001a;
        public static final int tvDrivingLicense=0x7f0b0006;
        public static final int tvMarriage=0x7f0b0011;
        public static final int tvPan=0x7f0b0014;
        public static final int tvPassport=0x7f0b0016;
        public static final int tvPoliceDonts=0x7f0b0019;
        public static final int tvPoliceDos=0x7f0b001c;
        public static final int tvPropertyBuy=0x7f0b0020;
        public static final int tvPropertyRent=0x7f0b0023;
        public static final int tvPropertySell=0x7f0b0025;
        public static final int tvTrafficPoliceFAQ=0x7f0b0029;
        public static final int tvTransportBuy2WheelSecond=0x7f0b002f;
        public static final int tvTransportNOC=0x7f0b0036;
        public static final int tvTransportbuy4Wheeler=0x7f0b0031;
        public static final int tvVote=0x7f0b0038;
    }
    public static final class layout {
        public static final int activity_aadhar_home=0x7f030000;
        public static final int activity_advance_search=0x7f030001;
        public static final int activity_disclaimer=0x7f030002;
        public static final int activity_driving_license_home=0x7f030003;
        public static final int activity_id_home=0x7f030004;
        public static final int activity_main=0x7f030005;
        public static final int activity_marriage_home=0x7f030006;
        public static final int activity_others_home=0x7f030007;
        public static final int activity_pan_home=0x7f030008;
        public static final int activity_passport_home=0x7f030009;
        public static final int activity_police_donts=0x7f03000a;
        public static final int activity_police_dos=0x7f03000b;
        public static final int activity_police_home=0x7f03000c;
        public static final int activity_property_buying=0x7f03000d;
        public static final int activity_property_home=0x7f03000e;
        public static final int activity_property_renting=0x7f03000f;
        public static final int activity_property_selling=0x7f030010;
        public static final int activity_search=0x7f030011;
        public static final int activity_splash_screen=0x7f030012;
        public static final int activity_traffic_police_faq=0x7f030013;
        public static final int activity_traffic_police_fines=0x7f030014;
        public static final int activity_traffic_police_home=0x7f030015;
        public static final int activity_transport_buy2wheeler_second=0x7f030016;
        public static final int activity_transport_buy4wheeler=0x7f030017;
        public static final int activity_transport_home=0x7f030018;
        public static final int activity_transport_noc=0x7f030019;
        public static final int activity_vote_home=0x7f03001a;
        public static final int advance_search_item_row=0x7f03001b;
        public static final int home_page=0x7f03001c;
        public static final int listview_item_row=0x7f03001d;
        public static final int searchview_item_row=0x7f03001e;
    }
    public static final class menu {
        public static final int advance_search=0x7f0a0000;
        public static final int driving_license_home=0x7f0a0001;
        public static final int id_home=0x7f0a0002;
        public static final int idhome=0x7f0a0003;
        public static final int main=0x7f0a0004;
        public static final int police_dos=0x7f0a0005;
        public static final int search_menu=0x7f0a0006;
        public static final int trafficpolicefinesfragment=0x7f0a0007;
    }
    public static final class raw {
        public static final int aadhar=0x7f050000;
        public static final int buying2wheeler_second_transport=0x7f050001;
        public static final int buying4wheeler_transport=0x7f050002;
        public static final int disclaimer=0x7f050003;
        public static final int driving_license=0x7f050004;
        public static final int marriage=0x7f050005;
        public static final int noc_transport=0x7f050006;
        public static final int pan=0x7f050007;
        public static final int passport=0x7f050008;
        public static final int police_donts=0x7f050009;
        public static final int police_dos=0x7f05000a;
        public static final int property_buying=0x7f05000b;
        public static final int property_renting=0x7f05000c;
        public static final int property_selling=0x7f05000d;
        public static final int traffic_faq=0x7f05000e;
        public static final int traffic_fines=0x7f05000f;
        public static final int vote=0x7f050010;
    }
    public static final class string {
        public static final int Aadhar=0x7f07000a;
        public static final int Aadhar_Card=0x7f070008;
        public static final int Buying=0x7f070010;
        public static final int Buying_Selling=0x7f07000f;
        public static final int Disclaimer=0x7f070027;
        public static final int Do_s=0x7f07000d;
        public static final int Dont_s=0x7f07000e;
        public static final int Driving_License=0x7f070006;
        public static final int IDs=0x7f070005;
        public static final int Marriage_Certificate=0x7f07000b;
        public static final int NOC=0x7f07001c;
        public static final int Others=0x7f07001e;
        public static final int PAN_Card=0x7f070007;
        public static final int Passport=0x7f070009;
        public static final int Police=0x7f070013;
        public static final int Police_Donts=0x7f070015;
        public static final int Police_Dos=0x7f070014;
        public static final int Property=0x7f07001d;
        public static final int Renting=0x7f070012;
        public static final int Search=0x7f070029;
        public static final int Selling=0x7f070011;
        public static final int Tax_Return=0x7f07001f;
        public static final int Traffic_Police=0x7f070016;
        public static final int Traffic_Police_FAQ=0x7f070018;
        public static final int Traffic_Police_Fines=0x7f070017;
        public static final int Transport=0x7f07001b;
        public static final int Transport_Buy_2Wheeler_Second=0x7f07001a;
        public static final int Transport_Buy_4Wheeler=0x7f070019;
        public static final int Update=0x7f070028;
        public static final int Voter_Card=0x7f07000c;
        public static final int action_settings=0x7f070001;
        public static final int app_name=0x7f070000;
        public static final int drawer_close=0x7f070004;
        public static final int drawer_open=0x7f070003;
        public static final int hello_world=0x7f070002;
        public static final int title_activity_advance_search=0x7f07002a;
        public static final int title_activity_driving_license_home=0x7f070022;
        public static final int title_activity_idhome=0x7f070020;
        public static final int title_activity_police_donts=0x7f070024;
        public static final int title_activity_police_dos=0x7f070023;
        public static final int title_activity_police_home=0x7f070021;
        public static final int title_activity_search=0x7f070026;
        public static final int title_activity_trafficpolicefinesfragment=0x7f070025;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f090000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f090001;
    }
    public static final class xml {
        public static final int searchable=0x7f040000;
    }
}
