package com.example.righttoknow;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.os.Bundle;
import android.app.Fragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.righttoknow.R;

	public class PropertyRentFragment extends Fragment{

		@Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	 
	        View rootView = inflater.inflate(R.layout.activity_property_renting, container, false);
	        this.getActivity().getActionBar().setTitle("Property");
	        //Find the view by its id
	        TextView tvPropertyRent = (TextView)rootView.findViewById(R.id.tvPropertyRent);
	      
	        InputStream inputStream = rootView.getContext().getResources().openRawResource(R.raw.property_renting);

	        InputStreamReader inputreader = new InputStreamReader(inputStream);
	        BufferedReader buffreader = new BufferedReader(inputreader);
	        String line;
	        StringBuilder text = new StringBuilder();

	        try {
	            while (( line = buffreader.readLine()) != null) {
	                text.append(line);
	                text.append('\n');
	            }
	        } catch (IOException e) {
	            return null;
	        }
	       
	        tvPropertyRent.setText(Html.fromHtml(text.toString()));
	        tvPropertyRent.setMovementMethod(LinkMovementMethod.getInstance());
		        return rootView;
		    }
		}
