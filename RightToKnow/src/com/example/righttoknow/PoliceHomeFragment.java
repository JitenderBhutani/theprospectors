package com.example.righttoknow;

import android.os.Bundle;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import com.example.righttoknow.R;

	public class PoliceHomeFragment extends Fragment implements OnClickListener{

	Button policeDos;
	Button policeDonts;
	Intent intent;
		
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.activity_police_home, container, false);
        this.getActivity().getActionBar().setTitle("Police");
        policeDos  = (Button)rootView.findViewById(R.id.PoliceDOs);
        policeDos.setOnClickListener(this);
        
        policeDonts  = (Button)rootView.findViewById(R.id.PoliceDonts);
        policeDonts.setOnClickListener(this);
 	       
	        return rootView;
	    }

	@Override
	public void onClick(View arg0) {
		
		Fragment fragment = null;
	    
		switch (arg0.getId())
	       {
	       
				case R.id.PoliceDOs:
					fragment = new PoliceDosFragment();
					break;
					
				case R.id.PoliceDonts:
					fragment = new PoliceDontsFragment();
					break;
				

				
		
	       }
	    if (fragment != null) {
	        FragmentManager fragmentManager = getFragmentManager();
	        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack(null).commit();
	         
	    } else {
	        Log.e("IdHomeActivity", "Error in creating fragment");
	    }
		
		
		
		
	}
	}
