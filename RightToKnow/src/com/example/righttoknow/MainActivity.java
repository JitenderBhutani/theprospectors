package com.example.righttoknow;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;

import android.os.Bundle;
import android.os.Handler;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;


import com.example.righttoknow.R;

public class MainActivity extends Activity {

	private String[] mNavigationDrawerItemTitles;
	private String[] searchItems;
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	ActionBarDrawerToggle mDrawerToggle;
	private CharSequence mDrawerTitle;
	private CharSequence mTitle;
	ObjectDrawerItem[] drawerItem;
	DrawerItemCustomAdapter drawerItemAdapter ;
	boolean doubleBackToExitPressedOnce = false;
	Fragment fragment = null;
	 FragmentManager fragmentManager;
	 ArrayList<String> alResourceFileCurrent;
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Fragment fragmentHome = new HomePageFragment();
        if (fragmentHome != null) {
	         fragmentManager = getFragmentManager();
	        fragmentManager.beginTransaction().replace(R.id.content_frame, fragmentHome).commit();
	             		        
	        //mDrawerList.setItemChecked(0, true);
	        //mDrawerList.setSelection(0);
	        setTitle("Home");
	        //mDrawerLayout.closeDrawer(0);
	        
	    } 
		
		mTitle = mDrawerTitle = getTitle();
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerToggle = new ActionBarDrawerToggle(
		        this,
		        mDrawerLayout,
		        R.drawable.ic_drawer, 
		        R.string.drawer_open, 
		        R.string.drawer_close 
		        ) {
		    
		    /** Called when a drawer has settled in a completely closed state. */
		    public void onDrawerClosed(View view) {
		        super.onDrawerClosed(view);
		        getActionBar().setTitle(mTitle);
		    }
		 
		    /** Called when a drawer has settled in a completely open state. */
		    public void onDrawerOpened(View drawerView) {
		        super.onDrawerOpened(drawerView);
		        getActionBar().setTitle(mDrawerTitle);
		    }
		};
		 
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		 
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		searchItems = getResources().getStringArray(R.array.search_items_array);
		
		mNavigationDrawerItemTitles= getResources().getStringArray(R.array.navigation_drawer_items_array);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);
		
		drawerItem = new ObjectDrawerItem[9];
		 
		drawerItem[0] = new ObjectDrawerItem(mNavigationDrawerItemTitles[0],R.drawable.postofficeicon);
		drawerItem[1] = new ObjectDrawerItem(mNavigationDrawerItemTitles[1],R.drawable.police);
		drawerItem[2] = new ObjectDrawerItem(mNavigationDrawerItemTitles[2],R.drawable.trafficpolice);
		drawerItem[3] = new ObjectDrawerItem(mNavigationDrawerItemTitles[3],R.drawable.id);
		drawerItem[4] = new ObjectDrawerItem(mNavigationDrawerItemTitles[4],R.drawable.transport);
		drawerItem[5] = new ObjectDrawerItem(mNavigationDrawerItemTitles[5],R.drawable.house);
		drawerItem[6] = new ObjectDrawerItem(mNavigationDrawerItemTitles[6],R.drawable.add);
		drawerItem[7] = new ObjectDrawerItem(mNavigationDrawerItemTitles[7],R.drawable.search2);
		drawerItem[8] = new ObjectDrawerItem(mNavigationDrawerItemTitles[8],R.drawable.disclaimer);
		
		
		drawerItemAdapter = new DrawerItemCustomAdapter(this, R.layout.listview_item_row, drawerItem);
		mDrawerList.setAdapter(drawerItemAdapter);
	

		if(getIntent().getExtras()!=null && getIntent().getExtras().containsKey("fromSearch")){
			String strSearch = getIntent().getExtras().getString("fromSearch");
			int index=0;
			
			int length = searchItems.length;
			
			for(int i=0;i<length;i++)
			{
				
				if(searchItems[i].toLowerCase().equals(strSearch.toLowerCase())){
					index = i;
					break;
				}

			}

			//Remove Search and disclaimer from search
			if(index>=7){
				index=index+2;
			}						
			DrawerItemClickListener obj = new DrawerItemClickListener();
			obj.selectItem(index);
		}
		
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		

	    return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    
	  if (mDrawerToggle.onOptionsItemSelected(item)) {
		   
	       return true;
	  }else{
		  switch (item.getItemId()) {
		case R.id.search:
			Intent intent = new Intent(MainActivity.this,SearchActivity.class);
	        startActivity(intent);
	        finish();
			break;
		/*case R.id.update:
			if(performUpdate()){
				Toast.makeText(this, "Update Successfully", Toast.LENGTH_SHORT).show();
			}else
			{
				Toast.makeText(this, "Update Fail", Toast.LENGTH_SHORT).show();				
			}
			break;*/
		default:
			break;
		}
			
	  }
		
	   return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void setTitle(CharSequence title) {
	    mTitle = title;
	    getActionBar().setTitle(mTitle);
	}
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
	    super.onPostCreate(savedInstanceState);
	    mDrawerToggle.syncState();
	}

	class DrawerItemClickListener implements ListView.OnItemClickListener {
	    
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        selectItem(position);
    }
	    
	 
	 private void selectItem(int position) {
	    
		 Intent intent;
	  		    
	    switch (position) {
	    
			    case 0:
				       	fragment = new HomePageFragment();
				        break;	
			    case 1:
			    		fragment = new PoliceHomeFragment();
			    		break;	    		
			    case 2:
			    		fragment = new TrafficPoliceHomeFragment();
			    		break;	    		
			    case 3:
			    		fragment = new IdHomeFragment();
			    		break;		    		
			    case 4:
			    		fragment = new TransportHomeFragment();
			    		break;		    		
			    case 5:
				    	intent = new Intent(MainActivity.this,PropertyHomeFragment.class);
			            startActivity(intent);
			            finish();
			            break;	            
			    case 6:
			    		fragment = new OthersHomeFragment();
			    		break;		
			    case 7:
			    		//fragment = new OthersHomeFragment();
				    	intent = new Intent(MainActivity.this,SearchActivity.class);
			            startActivity(intent);
			            finish();
			            break;
			    case 8:
		    			fragment = new DisclaimerFragment();
		    			break;
			    case 9:
				       	fragment = new PoliceDosFragment();
				        break;		
			    case 10:
			    		fragment = new PoliceDontsFragment();
			    		break;	    		
			    case 11:
			    		fragment = new TrafficpoliceFAQfragment();
			    		break;	    		
			    case 12:
			    		fragment = new Trafficpolicefinesfragment();
			    		break;		    		
			    case 13:
			    		fragment = new DrivingLicenseHome();
			    		break;		    		
			    case 14:
			    		fragment = new PANHomeFragment();
			            break;	            
			    case 15:
			    		fragment = new AadharHomeFragment();
			    		break;		
			    case 16:
			    		fragment = new PassportHomeFragment();
			            break;
			    case 17:
		    			fragment = new MarriageHomeFragment();
		    			break;
			    case 18:
			       		fragment = new VoteHomeFragment();
			       		break;	
			    case 19:
			    		fragment = new TransportBuy4WheelerFragment();
			    		break;	    		
			    case 20:
			    		fragment = new TransportBuy2WheelerSecondFragment();
			    		break;	    		
			    case 21:
			    		fragment = new TransportNOCFragment();
			    		break;		    		
			    case 22:
				    	intent = new Intent(MainActivity.this,PropertyHomeFragment.class);
				    	intent.putExtra("Tab", "Buy");
			            startActivity(intent);
			            finish();
			    		break;		    		
			    case 23:
				    	intent = new Intent(MainActivity.this,PropertyHomeFragment.class);
				    	intent.putExtra("Tab", "Sell");
			            startActivity(intent);
			            finish();
			            break;	            
			    case 24:
				    	intent = new Intent(MainActivity.this,PropertyHomeFragment.class);
				    	intent.putExtra("Tab", "Rent");
			            startActivity(intent);
			            finish();
			    		break;		
		    	default:
			   			break;
	    }
	    
	    if (fragment != null) {
	       fragmentManager = getFragmentManager();
	        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack(null).commit();
	         
	        mDrawerList.setItemChecked(position, true);
	        mDrawerList.setSelection(position);
	        
	        if(position>7){
	        	setTitle(searchItems[position]);
	        }else{
	        setTitle(mNavigationDrawerItemTitles[position]);
	        }
	        
	        mDrawerLayout.closeDrawer(mDrawerList);
	        
	    } else {
	        //Log.e("MainActivity", "Error in creating fragment");
	    }
	 }
	}
	
	@Override
	public void onBackPressed() {
	    if (doubleBackToExitPressedOnce) {
	    	System.exit(0);
	        //super.onBackPressed();
	        return;
	    }

	    if (fragmentManager.findFragmentById(R.id.content_frame) instanceof HomePageFragment){
	    	
	
	    this.doubleBackToExitPressedOnce = true;
	    Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

	    new Handler().postDelayed(new Runnable() {

	        @Override
	        public void run() {
	            doubleBackToExitPressedOnce=false;                       
	        }
	    }, 2000);
	} 
	    else
	    	 super.onBackPressed();
    }
    
	public boolean performUpdate(){
	
	    alResourceFileCurrent = new ArrayList<String>();
		
		//InputStream inputStream = this.getResources().openRawResource(R.raw.police_dos);
		Field[] id= R.raw.class.getFields();
				
		int length = id.length;
		
		for(int i=0;i<length;i++){
			alResourceFileCurrent.add(id[i].getName());
		}
		
		
		return true;
	}

}
