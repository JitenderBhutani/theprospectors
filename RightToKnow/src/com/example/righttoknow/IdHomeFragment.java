package com.example.righttoknow;

import android.os.Bundle;
import android.app.Fragment;
import android.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import com.example.righttoknow.R;

	public class IdHomeFragment extends Fragment implements OnClickListener{

	Button drivingLicenseButton,passportButton,btnAadhar,btnPAN,btnMarriage,btnVote;	
	
		
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.activity_id_home, container, false);
        this.getActivity().getActionBar().setTitle("ID");
	        drivingLicenseButton  = (Button)rootView.findViewById(R.id.Driving_License_Button);
	        drivingLicenseButton.setOnClickListener(this);
	        passportButton  = (Button)rootView.findViewById(R.id.Passport_Button);
	        passportButton.setOnClickListener(this);
	        btnAadhar  = (Button)rootView.findViewById(R.id.btnAadhar);
	        btnAadhar.setOnClickListener(this);
	        btnPAN  = (Button)rootView.findViewById(R.id.btnPAN);
	        btnPAN.setOnClickListener(this);
	        btnMarriage  = (Button)rootView.findViewById(R.id.btnMarriage);
	        btnMarriage.setOnClickListener(this);
	        btnVote  = (Button)rootView.findViewById(R.id.btnVote);
	        btnVote.setOnClickListener(this);
	        
	        return rootView;
	    }

	@Override
	public void onClick(View arg0) {
		
		Fragment fragment = null;
		    
		switch (arg0.getId())
	       {
	       
				case R.id.Driving_License_Button:
					fragment = new DrivingLicenseHome();	
					break;
				case R.id.Passport_Button:
					fragment = new PassportHomeFragment();	
					break;
				case R.id.btnAadhar:
					fragment = new AadharHomeFragment();	
					break;
				case R.id.btnPAN:
					fragment = new PANHomeFragment();	
					break;	
				case R.id.btnVote:
					fragment = new VoteHomeFragment();	
					break;	
				case R.id.btnMarriage:
					fragment = new MarriageHomeFragment();	
					break;
	       }
	    if (fragment != null) {
	        FragmentManager fragmentManager = getFragmentManager();
	        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack(null).commit();
	         
	    } else {
	        Log.e("IdHomeActivity", "Error in creating fragment");
	    }
	 }
	
	}
