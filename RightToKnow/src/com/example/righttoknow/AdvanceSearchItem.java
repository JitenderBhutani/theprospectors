package com.example.righttoknow;

public class AdvanceSearchItem {
	 public String fragment;
	 public String text;
	 
    // Constructor.
    public AdvanceSearchItem(String fragment,String text) {
         this.fragment = fragment;
         this.text = text;
    }
}
