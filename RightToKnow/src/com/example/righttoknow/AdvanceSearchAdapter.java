package com.example.righttoknow;

import com.example.righttoknow.R;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class AdvanceSearchAdapter extends ArrayAdapter<AdvanceSearchItem> {
	 
    Context mContext;
    int layoutResourceId;
    AdvanceSearchItem data[] = null;
 
    public AdvanceSearchAdapter(Context mContext, int layoutResourceId, AdvanceSearchItem[] data) {
 
    	super(mContext, layoutResourceId, data);
    	this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
 
        View listItem = convertView;
 
        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        listItem = inflater.inflate(layoutResourceId, parent, false);
 
        TextView textViewName = (TextView) listItem.findViewById(R.id.tvASFragment);
        TextView textText = (TextView) listItem.findViewById(R.id.tvASText);
        
        AdvanceSearchItem folder = data[position];
 
        textViewName.setText(folder.fragment);
        textText.setText(Html.fromHtml(folder.text));
        
        return listItem;
    }
 
}