package com.example.righttoknow;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.os.Bundle;
import android.app.Fragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.righttoknow.R;

	public class PoliceDosFragment extends Fragment{

		@Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	 
	        View rootView = inflater.inflate(R.layout.activity_police_dos, container, false);
	        this.getActivity().getActionBar().setTitle("Police");
	        //Find the view by its id
	        TextView tvPoliceDos = (TextView)rootView.findViewById(R.id.tvPoliceDos);
	      
	        InputStream inputStream = rootView.getContext().getResources().openRawResource(R.raw.police_dos);

	        InputStreamReader inputreader = new InputStreamReader(inputStream);
	        BufferedReader buffreader = new BufferedReader(inputreader);
	        String line;
	        StringBuilder text = new StringBuilder();

	        try {
	            while (( line = buffreader.readLine()) != null) {
	                text.append(line);
	                text.append('\n');
	            }
	        } catch (IOException e) {
	            return null;
	        }
	
	       tvPoliceDos.setText(Html.fromHtml(text.toString()));	       
	       tvPoliceDos.setMovementMethod(LinkMovementMethod.getInstance());
	       
		        return rootView;
		    }
		}
