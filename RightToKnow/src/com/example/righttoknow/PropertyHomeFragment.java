package com.example.righttoknow;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;

import com.example.righttoknow.R;
import com.example.righttoknow.MainActivity.DrawerItemClickListener;


public class PropertyHomeFragment extends Activity{

	ActionBar.Tab Tab1, Tab2, Tab3;
	Fragment propertyBuyFragment = new PropertyBuyFragment();
	Fragment propertySellFragment = new PropertySellFragment();
	Fragment propertyRentFragment = new PropertyRentFragment();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_property_home);
		
		getActionBar().setTitle("Property");
        
		ActionBar actionBar = getActionBar();
        
		// Hide Actionbar Icon
		actionBar.setDisplayShowHomeEnabled(true);
 
		// Hide Actionbar Title
		actionBar.setDisplayShowTitleEnabled(true);
 
		// Create Actionbar Tabs
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
 
		// Set Tab Icon and Titles
		Tab1 = actionBar.newTab().setText("Buying");
		Tab2 = actionBar.newTab().setText("Selling");
		Tab3 = actionBar.newTab().setText("Renting");
 
		// Set Tab Listeners
		Tab1.setTabListener(new TabListener(propertyBuyFragment));
		Tab2.setTabListener(new TabListener(propertySellFragment));
		Tab3.setTabListener(new TabListener(propertyRentFragment));
 
		// Add tabs to actionbar
		actionBar.addTab(Tab1);
		actionBar.addTab(Tab2);
		actionBar.addTab(Tab3);
		
		if(getIntent().getExtras()!=null && getIntent().getExtras().containsKey("Tab")){
			String strSearch = getIntent().getExtras().getString("Tab");
			
			if(strSearch.equals("Buy")){				
				actionBar.selectTab(Tab1);
			}
			else if (strSearch.equals("Sell")){
				actionBar.selectTab(Tab2);
			}
			else if (strSearch.equals("Rent")){
				actionBar.selectTab(Tab3);
			}
		}
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main, menu);
		
	    return true;
	}
	 @Override
		public void onBackPressed() {
		 Intent intent = new Intent(PropertyHomeFragment.this,MainActivity.class);
	        startActivity(intent);
	        finish();
			
		}
	
}
