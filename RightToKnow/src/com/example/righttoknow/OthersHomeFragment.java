package com.example.righttoknow;

import com.example.righttoknow.R;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class OthersHomeFragment extends Fragment{

	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.activity_others_home, container, false);
        this.getActivity().getActionBar().setTitle("Others");  
	        return rootView;
	    }
	}
