package com.example.righttoknow;

import com.example.righttoknow.R;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class HomePageFragment extends Fragment implements OnClickListener{

	Button btnPolice,btnTrafficPolice,btnID,btnTransport,btnProperty,btnOthers;
	View rootView;
	String title;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
 
        rootView = inflater.inflate(R.layout.home_page, container, false);
        
        this.getActivity().getActionBar().setTitle("Home");
        
        btnPolice = (Button)rootView.findViewById(R.id.btnPolice);
        btnTrafficPolice = (Button)rootView.findViewById(R.id.btnTrafficPolice);
        btnID = (Button)rootView.findViewById(R.id.btnID);
        btnTransport = (Button)rootView.findViewById(R.id.btnTransport);
        btnProperty = (Button)rootView.findViewById(R.id.btnProperty);
        btnOthers = (Button)rootView.findViewById(R.id.btnOthers);
 	       
        btnPolice.setOnClickListener(this);
        btnTrafficPolice.setOnClickListener(this);
        btnID.setOnClickListener(this);
        btnTransport.setOnClickListener(this);
        btnProperty.setOnClickListener(this);
        btnOthers.setOnClickListener(this);
        
	        return rootView;
	    }
	
	@Override
	public void onClick(View arg0) {
		
		Fragment fragment = null;
	    		
		switch(arg0.getId()){
		
		case R.id.btnPolice:
			fragment = new PoliceHomeFragment();
			title = "Police";
			break;
			
		case R.id.btnTrafficPolice:
			fragment = new TrafficPoliceHomeFragment();
			title = "Traffic Police";
			break;
			
		case R.id.btnID:
			fragment = new IdHomeFragment();
			title = "ID";
			break;
			
		case R.id.btnTransport:
			fragment = new TransportHomeFragment();
			title = "Transport";
			break;
			
		case R.id.btnProperty:
			Intent intent = new Intent(this.getActivity(),PropertyHomeFragment.class);
            startActivity(intent);
            this.getActivity().finish();
            break;
			
		case R.id.btnOthers:
			fragment = new OthersHomeFragment();
			title = "Others";
			break;	
		
		}
		
		if (fragment != null) {
	        FragmentManager fragmentManager = getFragmentManager();
	        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack(null).commit();
	        
	        this.getActivity().getActionBar().setTitle(title);
	        //rootView.getActionBar().setTitle("asdsa");
	         
	    } else {
	        //Log.e("HomePageFragment", "Error in creating fragment");
	    }
		
	}
	}
