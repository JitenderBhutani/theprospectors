package com.example.righttoknow;

import com.example.righttoknow.R;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;

public class TransportHomeFragment extends Fragment implements OnClickListener{

	Button btnNOC,btnBike,btnCar;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.activity_transport_home, container, false);
        this.getActivity().getActionBar().setTitle("Transport");
	        btnNOC  = (Button)rootView.findViewById(R.id.btnNOC);
	        btnNOC.setOnClickListener(this);
	        btnBike  = (Button)rootView.findViewById(R.id.btnBike);
	        btnBike.setOnClickListener(this);
	        btnCar  = (Button)rootView.findViewById(R.id.btnCar);
	        btnCar.setOnClickListener(this);
	       
	        return rootView;
	    }
	@Override
	public void onClick(View arg0) {
		
		Fragment fragment = null;
		    
		switch (arg0.getId())
	       {
	       
				case R.id.btnNOC:
					fragment = new TransportNOCFragment();	
					break;
				case R.id.btnBike:
					fragment = new TransportBuy2WheelerSecondFragment();	
					break;
				case R.id.btnCar:
					fragment = new TransportBuy4WheelerFragment();	
					break;
				
	       }
	    if (fragment != null) {
	        FragmentManager fragmentManager = getFragmentManager();
	        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack(null).commit();
	         
	    } else {
	        Log.e("TransportHomeActivity", "Error in creating fragment");
	    }
	 		
	 }
	
	}
