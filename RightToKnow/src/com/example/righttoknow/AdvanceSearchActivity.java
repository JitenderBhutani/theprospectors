package com.example.righttoknow;

import java.util.ArrayList;

import com.example.righttoknow.MainActivity.DrawerItemClickListener;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class AdvanceSearchActivity extends Activity implements  OnItemClickListener {

	AdvanceSearchItem[] searchItem;
	AdvanceSearchAdapter searchItemAdapter ;
	private ListView searchList;
	ArrayList<String> strFragment;
	ArrayList<String> strText;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_advance_search);
		
		
		if(getIntent().getExtras()!=null && getIntent().getExtras().containsKey("Fragment")){			
			 strFragment = getIntent().getExtras().getStringArrayList("Fragment");
			 strText = getIntent().getExtras().getStringArrayList("Text");			
		}
		
		//searchItem = new AdvanceSearchItem[1];
		searchList = (ListView) findViewById(R.id.lvAS);
		
		int length = strFragment.size();
		
		searchItem = new AdvanceSearchItem[length];
		
		for(int i=0;i<length;i++)
		{
			//searchItem[i] = new AdvanceSearchItem();
			searchItem[i] = new AdvanceSearchItem(strFragment.get(i).toString(),strText.get(i).toString());
		}
		
		
		searchItemAdapter = new AdvanceSearchAdapter(this, R.layout.advance_search_item_row, searchItem);
		searchList.setAdapter(searchItemAdapter);
		searchList.setOnItemClickListener(this);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.advance_search, menu);
		return true;
	}
	@Override
	public void onBackPressed() {
    
    	Intent intent = new Intent(AdvanceSearchActivity.this,SearchActivity.class);
        startActivity(intent);
        finish();		
		
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

		Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("fromSearch", strFragment.get(arg2));
        this.startActivity(intent);	
        finish();
		
	}

}
