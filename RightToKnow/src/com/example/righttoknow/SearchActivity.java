package com.example.righttoknow;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;

import android.R.string;
import android.os.Bundle;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;
import android.widget.SearchView.OnQueryTextListener;

public class SearchActivity extends Activity implements OnQueryTextListener, OnItemClickListener {


	private ListView friendListView;
    private SearchView searchView;
    private MenuItem searchMenuItem;
    private FriendListAdapter friendListAdapter;
    private ArrayList<String> friendList = new ArrayList<String>();
    private String[] searchItems;
    HashMap<String, String> hmAdvanceSearch;
    ArrayList<String> arFragment = new ArrayList<String>();
    ArrayList<String> arText = new ArrayList<String>();
   // private AdvanceSearchAdapter advanceSearchAdapter;
    //private HashMap<String,String> advanceList = new HashMap<String,String>();
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);
		initFriendList();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.search_menu, menu);

	    SearchManager searchManager = (SearchManager)
	                            getSystemService(Context.SEARCH_SERVICE);
	    searchMenuItem = menu.findItem(R.id.search);
	    searchView = (SearchView) searchMenuItem.getActionView();

	    searchView.setSearchableInfo(searchManager.
	                            getSearchableInfo(getComponentName()));
	    searchView.setSubmitButtonEnabled(true);
	   // searchView.setFocusable(true);
	    //searchView.setQueryHint("Type Here");
	    //searchView.setSelected(true);
	    searchView.setOnQueryTextListener(this);
		return true;
	}

	
	@Override
	public boolean onQueryTextChange(String newText) {
		
		 friendListAdapter.getFilter().filter(newText);
		//searchCommand.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
		return true;
	}

	@Override
	public boolean onQueryTextSubmit(String newText) {
		
		advanceSearch(newText);
		if(hmAdvanceSearch.size()!=0){
				Intent intent = new Intent(this, AdvanceSearchActivity.class);
							
				intent.putStringArrayListExtra("Fragment",arFragment);
				intent.putStringArrayListExtra("Text",arText);
		        this.startActivity(intent);
		        finish();
		}else{
			Toast.makeText(this, "No Results", Toast.LENGTH_SHORT).show();
		}
				
		return true;
	}
	
	private void handelListItemClick(String user) {
	    // close search view if its visible
	    if (searchView.isShown()) {
	        searchMenuItem.collapseActionView();
	        searchView.setQuery("", false);
	    }
	 
	    Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("fromSearch", user);
        this.startActivity(intent);

	}
	
	/**
     * Initialize friend list
     */
    private void initFriendList() {
       // friendList = application.getContactList();
    	searchItems= getResources().getStringArray(R.array.search_items_array);

    	for(String str : searchItems){
    			friendList.add(str);
		}
		
		
        friendListView = (ListView) findViewById(R.id.friend_list);
        friendListAdapter = new FriendListAdapter(this, friendList);

        // add header and footer for list
       // View headerView = View.inflate(this, R.layout., null);
       // View footerView = View.inflate(this, R.layout.list_header, null);
        //friendListView.addHeaderView(headerView);
      //  friendListView.addFooterView(footerView);
        friendListView.setAdapter(friendListAdapter);
        friendListView.setTextFilterEnabled(true);

        // use to enable search view popup text
        friendListView.setTextFilterEnabled(true);

        // set up click listener
        friendListView.setOnItemClickListener(this);
        //this.finish();
    }
    
    
    @Override
	public void onBackPressed() {
    
    	Intent intent = new Intent(SearchActivity.this,MainActivity.class);
        startActivity(intent);
        finish();
		
		
	}
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    	
        if(position>=0 && position <= friendList.size()) {
            handelListItemClick((String)friendListAdapter.getItem(position));
            this.finish();
        }
        
    }
    
    public HashMap<String, String> advanceSearch(String newText){
    	
    	hmAdvanceSearch = new  HashMap<String, String>();
		
		InputStream inputStream = this.getResources().openRawResource(R.raw.police_dos);
		InputStream inputStream2 = this.getResources().openRawResource(R.raw.police_donts);
		InputStream inputStream3 = this.getResources().openRawResource(R.raw.traffic_fines);
		InputStream inputStream4 = this.getResources().openRawResource(R.raw.traffic_faq);
		InputStream inputStream5 = this.getResources().openRawResource(R.raw.driving_license);
		InputStream inputStream6 = this.getResources().openRawResource(R.raw.passport);
		InputStream inputStream7 = this.getResources().openRawResource(R.raw.pan);
		InputStream inputStream8 = this.getResources().openRawResource(R.raw.aadhar);
		InputStream inputStream9 = this.getResources().openRawResource(R.raw.vote);
		InputStream inputStream10 = this.getResources().openRawResource(R.raw.marriage);
		InputStream inputStream11 = this.getResources().openRawResource(R.raw.buying2wheeler_second_transport);
		InputStream inputStream12 = this.getResources().openRawResource(R.raw.buying4wheeler_transport);
		InputStream inputStream13 = this.getResources().openRawResource(R.raw.noc_transport);
		InputStream inputStream14 = this.getResources().openRawResource(R.raw.property_buying);
		InputStream inputStream15 = this.getResources().openRawResource(R.raw.property_selling);
		InputStream inputStream16 = this.getResources().openRawResource(R.raw.property_renting);
		//InputStream inputStream17 = this.getResources().openRawResource(R.raw.police_dos);
		
		
		//InputStreamReader inputreader[] = new InputStreamReader();
		InputStreamReader inputreader = new InputStreamReader(inputStream);
		InputStreamReader inputreader2 = new InputStreamReader(inputStream2);
		InputStreamReader inputreader3 = new InputStreamReader(inputStream3);
		InputStreamReader inputreader4 = new InputStreamReader(inputStream4);
		InputStreamReader inputreader5 = new InputStreamReader(inputStream5);
		InputStreamReader inputreader6 = new InputStreamReader(inputStream6);
		InputStreamReader inputreader7 = new InputStreamReader(inputStream7);
		InputStreamReader inputreader8 = new InputStreamReader(inputStream8);
		InputStreamReader inputreader9 = new InputStreamReader(inputStream9);
		InputStreamReader inputreader10 = new InputStreamReader(inputStream10);
		InputStreamReader inputreader11 = new InputStreamReader(inputStream11);
		InputStreamReader inputreader12 = new InputStreamReader(inputStream12);
		InputStreamReader inputreader13 = new InputStreamReader(inputStream13);
		InputStreamReader inputreader14 = new InputStreamReader(inputStream14);
		InputStreamReader inputreader15 = new InputStreamReader(inputStream15);
		InputStreamReader inputreader16 = new InputStreamReader(inputStream16);
		//InputStreamReader inputreader17 = new InputStreamReader(inputStream17);

		//BufferedReader buffreader = new BufferedReader(5)[5];
        BufferedReader buffreader = new BufferedReader(inputreader);
        BufferedReader buffreader2 = new BufferedReader(inputreader2);
        BufferedReader buffreader3 = new BufferedReader(inputreader3);
        BufferedReader buffreader4 = new BufferedReader(inputreader4);
        BufferedReader buffreader5 = new BufferedReader(inputreader5);
        BufferedReader buffreader6 = new BufferedReader(inputreader6);
        BufferedReader buffreader7 = new BufferedReader(inputreader7);
        BufferedReader buffreader8 = new BufferedReader(inputreader8);
        BufferedReader buffreader9 = new BufferedReader(inputreader9);
        BufferedReader buffreader10 = new BufferedReader(inputreader10);
        BufferedReader buffreader11 = new BufferedReader(inputreader11);
        BufferedReader buffreader12 = new BufferedReader(inputreader12);
        BufferedReader buffreader13 = new BufferedReader(inputreader13);
        BufferedReader buffreader14 = new BufferedReader(inputreader14);
        BufferedReader buffreader15 = new BufferedReader(inputreader15);
        BufferedReader buffreader16 = new BufferedReader(inputreader16);
        //BufferedReader buffreader17 = new BufferedReader(inputreader17);
        
        String line;
        StringBuilder text = new StringBuilder();

        try {
            while (( line = buffreader.readLine()) != null) {
            	if(line.toLowerCase().contains(newText.toLowerCase())){
            		arFragment.add("Police Do\'s");
                	arText.add(line);
                	hmAdvanceSearch.put("Police Do\'s", line);
                }
            }
            line=null;
            while (( line = buffreader2.readLine()) != null) {
            	if(line.toLowerCase().contains(newText.toLowerCase())){
            		arFragment.add("Police Dont\'s");
                	arText.add(line);
                	hmAdvanceSearch.put("Police Dont\'s", line);
                }
            }
            line=null;
            while (( line = buffreader3.readLine()) != null) {
            	if(line.toLowerCase().contains(newText.toLowerCase())){
            		arFragment.add("Traffic Police Fines");
                	arText.add(line);
                	hmAdvanceSearch.put("Traffic Police Fines", line);
                }
            }
            line=null;
            while (( line = buffreader4.readLine()) != null) {
            	if(line.toLowerCase().contains(newText.toLowerCase())){
            		arFragment.add("Traffic Police FAQ");
                	arText.add(line);
                	hmAdvanceSearch.put("Traffic Police FAQ", line);
                }
            }
            line=null;
            while (( line = buffreader5.readLine()) != null) {
            	if(line.toLowerCase().contains(newText.toLowerCase())){
            		arFragment.add("Driving License");
                	arText.add(line);
                	hmAdvanceSearch.put("Driving License", line);
                }
            }
            line=null;
            while (( line = buffreader6.readLine()) != null) {
            	if(line.toLowerCase().contains(newText.toLowerCase())){
            		arFragment.add("Passport");
                	arText.add(line);
                	hmAdvanceSearch.put("Passport", line);
                }
            }
            line=null;
            while (( line = buffreader7.readLine()) != null) {
            	if(line.toLowerCase().contains(newText.toLowerCase())){
            		arFragment.add("PAN Card");
                	arText.add(line);
                	hmAdvanceSearch.put("PAN Card", line);
                }
            }
            line=null;
            while (( line = buffreader8.readLine()) != null) {
            	if(line.toLowerCase().contains(newText.toLowerCase())){
            		arFragment.add("Aadhar Card");
                	arText.add(line);
                	hmAdvanceSearch.put("Aadhar Card", line);
                }
            }
            line=null;
            while (( line = buffreader9.readLine()) != null) {
            	if(line.toLowerCase().contains(newText.toLowerCase())){
            		arFragment.add("Voter Card");
                	arText.add(line);
                	hmAdvanceSearch.put("Voter Card", line);
                }
            }
            line=null;
            while (( line = buffreader10.readLine()) != null) {
            	if(line.toLowerCase().contains(newText.toLowerCase())){
            		arFragment.add("Marriage Certificate");
                	arText.add(line);
                	hmAdvanceSearch.put("Marriage Certificate", line);
                }
            }
            line=null;
            while (( line = buffreader11.readLine()) != null) {
            	if(line.toLowerCase().contains(newText.toLowerCase())){
            		arFragment.add("Buy 2-Wheeler Second Hand");
                	arText.add(line);
                	hmAdvanceSearch.put("Buy 2-Wheeler Second Hand", line);
                }
            }
            line=null;
            while (( line = buffreader12.readLine()) != null) {
            	if(line.toLowerCase().contains(newText.toLowerCase())){
            		arFragment.add("Buy 4-Wheeler");
                	arText.add(line);
                	hmAdvanceSearch.put("Buy 4-Wheeler", line);
                }
            }
            line=null;
            while (( line = buffreader13.readLine()) != null) {
            	if(line.toLowerCase().contains(newText.toLowerCase())){
            		arFragment.add("NOC");
                	arText.add(line);
                	hmAdvanceSearch.put("NOC", line);
                }
            }
            line=null;
            while (( line = buffreader14.readLine()) != null) {
            	if(line.toLowerCase().contains(newText.toLowerCase())){
            		arFragment.add("Property Buy");
                	arText.add(line);
                	hmAdvanceSearch.put("Property Buy", line);
                }
            }
            line=null;
            while (( line = buffreader15.readLine()) != null) {
            	if(line.toLowerCase().contains(newText.toLowerCase())){
            		arFragment.add("Property Sell");
                	arText.add(line);
                	hmAdvanceSearch.put("Property Sell", line);
                }
            }
            line=null;
            while (( line = buffreader16.readLine()) != null) {
            	if(line.toLowerCase().contains(newText.toLowerCase())){
            		arFragment.add("Property Rent");
                	arText.add(line);
                	hmAdvanceSearch.put("Property Rent", line);
                }
            }
            line=null;
           
        } catch (IOException e) {
           return null;
        }
       
		return hmAdvanceSearch;
    }
}

