package com.example.righttoknow;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.os.Bundle;
import android.app.Fragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.righttoknow.R;

	public class DrivingLicenseHome extends Fragment{

		
		@Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	 
	        View rootView = inflater.inflate(R.layout.activity_driving_license_home, container, false);
	        this.getActivity().getActionBar().setTitle("ID");
	      //Find the view by its id
	        TextView tvDrivingLicense = (TextView)rootView.findViewById(R.id.tvDrivingLicense);
	      
	        InputStream inputStream = rootView.getContext().getResources().openRawResource(R.raw.driving_license);

	        InputStreamReader inputreader = new InputStreamReader(inputStream);
	        BufferedReader buffreader = new BufferedReader(inputreader);
	        String line;
	        StringBuilder text = new StringBuilder();

	        try {
	            while (( line = buffreader.readLine()) != null) {
	                text.append(line);
	                text.append('\n');
	            }
	        } catch (IOException e) {
	            return null;
	        }
	       
	        tvDrivingLicense.setText(Html.fromHtml(text.toString()));
	        tvDrivingLicense.setMovementMethod(LinkMovementMethod.getInstance());
		        return rootView;
		    }
		}
