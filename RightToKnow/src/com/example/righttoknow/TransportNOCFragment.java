package com.example.righttoknow;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.os.Bundle;
import android.app.Fragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.righttoknow.R;

	public class TransportNOCFragment extends Fragment{

		@Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	 
	        View rootView = inflater.inflate(R.layout.activity_transport_noc, container, false);
	        this.getActivity().getActionBar().setTitle("Transport");
	        //Find the view by its id
	        TextView tvTransportNOC = (TextView)rootView.findViewById(R.id.tvTransportNOC);
	      
	        InputStream inputStream = rootView.getContext().getResources().openRawResource(R.raw.noc_transport);

	        InputStreamReader inputreader = new InputStreamReader(inputStream);
	        BufferedReader buffreader = new BufferedReader(inputreader);
	        String line;
	        StringBuilder text = new StringBuilder();

	        try {
	            while (( line = buffreader.readLine()) != null) {
	                text.append(line);
	                text.append('\n');
	            }
	        } catch (IOException e) {
	            return null;
	        }
	       
	        tvTransportNOC.setText(Html.fromHtml(text.toString()));
	        tvTransportNOC.setMovementMethod(LinkMovementMethod.getInstance());
		        return rootView;
		    }
		}
