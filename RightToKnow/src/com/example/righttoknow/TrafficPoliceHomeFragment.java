package com.example.righttoknow;

import android.os.Bundle;
import android.app.Fragment;
import android.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.righttoknow.R;

	public class TrafficPoliceHomeFragment extends Fragment implements OnClickListener{

	Button btntrafficFine,btntrafficFAQ;
		
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.activity_traffic_police_home, container, false);
        this.getActivity().getActionBar().setTitle("Traffic Police");
     
        btntrafficFine  = (Button)rootView.findViewById(R.id.btnTrafficFine);
        btntrafficFine.setOnClickListener(this);
        btntrafficFAQ  = (Button)rootView.findViewById(R.id.btnTrafficFAQ);
        btntrafficFAQ.setOnClickListener(this);
        
	        return rootView;
	    }

	@Override
	public void onClick(View arg0) {
		Fragment fragment = null;
	    
		switch (arg0.getId())
	       {
	       
				case R.id.btnTrafficFine:
					fragment = new Trafficpolicefinesfragment();	
					break;
							
				case R.id.btnTrafficFAQ:
					fragment = new TrafficpoliceFAQfragment();	
					break;					
					
	       }
	    if (fragment != null) {
	        FragmentManager fragmentManager = getFragmentManager();
	        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack(null).commit();
	         
	    } else {
	        Log.e("TrafficHomeFragment", "Error in creating fragment");
	    }
		
	}
	}
