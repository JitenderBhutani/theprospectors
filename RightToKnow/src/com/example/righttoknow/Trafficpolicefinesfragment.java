package com.example.righttoknow;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.os.Bundle;
import android.app.Fragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

	public class Trafficpolicefinesfragment extends Fragment{

	       
        ArrayList<String> alTrafficPoliceFines = new ArrayList<String>();
        TableLayout tableTrafficPoliceHome;
        TextView tvTrafficPoliceSNO,tvTrafficPoliceOffence,tvTrafficPolicePenalty,tvTrafficPoliceSection;
        TableRow row ;
        String[] textFile;
        
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.activity_traffic_police_fines, container, false);
        this.getActivity().getActionBar().setTitle("Traffic Police");
        //Find the view by its id
        InputStream inputStream = rootView.getContext().getResources().openRawResource(R.raw.traffic_fines);

        InputStreamReader inputreader = new InputStreamReader(inputStream);
        BufferedReader buffreader = new BufferedReader(inputreader);
        String line;
        //StringBuilder text = new StringBuilder();

        try {
        	 
            tableTrafficPoliceHome = (TableLayout)rootView.findViewById(R.id.tableTrafficPoliceFines);
            
            while (( line = buffreader.readLine()) != null) {
            	
                //text.append(line);
                textFile = line.toString().split("#");
                //text.append('\n');
                
            	row= new TableRow(this.getActivity());        	
            	
            	for(int j=0;j<4;j++){
            		tvTrafficPoliceSNO = new TextView(this.getActivity());
            		tvTrafficPoliceSNO.setText(Html.fromHtml(textFile[j].toString()));
            		tvTrafficPoliceSNO.setPadding(1, 2, 2, 2);
            		tvTrafficPoliceSNO.setBackgroundResource(R.drawable.cellstyle);
            		tvTrafficPoliceSNO.isTextSelectable();
            		tvTrafficPoliceSNO.setMovementMethod(LinkMovementMethod.getInstance());
            		//tvTrafficPoliceSNO.setLayoutParams(new LayoutParams(5, 5));
            		//tableTrafficPoliceHome.setColumnStretchable(j, true);
            		//tvTrafficPoliceSNO.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
                	row.addView(tvTrafficPoliceSNO);
                	//row.setLayoutParams(new LayoutParams(5, 5));
                	
            	}               	
            	
            	tableTrafficPoliceHome.addView(row);
                
            }
        	
            
        } catch (IOException e) {
            return null;
        }
       
      
        
	        return rootView;
	    }
	
	}
