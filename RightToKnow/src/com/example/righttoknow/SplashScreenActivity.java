package com.example.righttoknow;

import com.example.righttoknow.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;


public class SplashScreenActivity extends Activity {

	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        
        new Handler().postDelayed(new Runnable() {			
			@Override
			public void run() {
				// TODO Auto-generated method stub

				Intent i = new Intent(SplashScreenActivity.this,MainActivity.class);
				startActivity(i);
				finish();
				
				
			}
		}, 3000);
      

    }
       

}

